# nevha.docs

NevHA is a modern, 'distributed', 'containerized' and fast home automation harness.

While it does technically further the standards problem (https://xkcd.com/927/) - it does allow
development of multiple different clients. Be they web, mobile or embedded.

NevHA is purely an educational project and isn't meant to be a large-scale, production-ready project.

Anywho, in this repo you'll find information on the APIs and transport systems used by NevHA.

# The Plan

NevHA is built as an abstraction layer between [x smart home controller] and the interface.

The idea is one simple JSON format API can control a whole manner of different smart-home systems.

Each smart-home system implemented will get it's own module. Mostly based around the vendor of the hardware
such as Philips, IKEA or LIFX.

## Communication

In version 0.1 of NevHA, controllers will communicate with the registry via HTTP, just like the interfaces will communicate with controllers
when implemented.

MQTT will be used to send real-time state data to other controllers/registries.

When [a phone] wants to see the list of available devices, it first contacts [the registry], which returns a JSON array
of available devices. Each device will use the device object outlined below, and contain information about the controller
it is downstream of, [the phone] will then use device and controller information to send messages to the controller, which in turn will
control [the device]

### Issues with this approach

In some cases, the registry and controllers may exist on different VLANs, in this case, [the phone] may not be able to communicate
directly with the controller. A possible solution to this would be to write a proxy server that is connected to all networks and can
down/upstream data to/from controllers.

## State Tracking

Each controller will keep a state cache of all of it's downstream devices. It will collect the state of all of it's devices on startup
as part of the bootstrapping process, and then announce the states to the registry while registering all devices.

When the state of a downstream device is changed (be it by a user, or the device itself) a message will be published to
`nevha-state-track` on MQTT. This message will contain information about the device, and the state it changed to.

`{
    "device_id": "ABC1232",
    "event": "state_update",
    "old_state": {"power": "on"},
    "new_state": {"power": "off"}
}`

`old_state` is included for logging purposes, the new state should be added to the registry database as soon as possible.

And if implemented, broadcast a websocket message to all upstream interfacing devices (such as a mobile phone app).

## Device Object

To abstract from the complexity of the various smart home devices and protocols, a standard JSON object
will be used to identify any device on the NevHA stack. It will be formatted to the following standard

`
{
    "device_id": "ABC123",
    "device_friendly_name": "Lamp",
    "device_location": {
        "id": "living_room",
        "friendly_name": "Living Room"
    }
    "device_type": "nevha.lighting.white.bulb",
    "device_vendor": "IKEA",
    "controller": {
        "api_proxy": false,
        "host": "172.16.1.123",
        "port": 3000,
        "protocol": "nevha.http.v1"
    },
    "device_state": {
        // State Object
    }
}
`

## Device State object

Every device has a 'state' within both it's upstream controller and the registry.
The state object, like the device object, is standardized across all supported devices, and is accepted
by any controller/interface program.

### State: Lighting (* No Colour *)
`
{
    "power": "on/off",
    "brightness": {
        "overall_percent": 100
    }
}
`

### State: Lighting (* Colour *)
`
{
    "power": "on/off",
    "brightness": {
        "rgb": [255,255,255],
        "overall_percentage": 100
    }
}
`

### State: Smart Plug
`
{
    "power": "on/off",
    "power_draw": 40, // Watts (if supported) (read only)
    "current_draw": 0.5 // Amps (if supported) (read only)
}
`

### State: Smart Blinds
`
{
    "drawn_percentage": 100 // 0 - Open, 100 - Fully closed
}
`

### State: HVAC

HVAC is a broad term, it covers many different technologies. This state is the most likely to change over time.

`
{
    "temperature": {
        "target_temperature": 20, // Celsius
        "current_temperature": 19, // Read only
    },
    "heating": {
        "enable": true
    },
    "cooling": {
        "enable": true
    },
    "ventilation": {
        "enable": true,
        "fan_speed": 50
    }
}
`

### State: Vacuum Cleaners
`
{
    "active": true,
    "mode": "auto/spot/charge",
    "cycle_start": "iso timestamp",
    "next_cycle_start": "iso timestamp",
    "battery_percentage": 50
}
`

